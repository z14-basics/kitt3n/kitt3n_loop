<?php
namespace KITT3N\Kitt3nLoop\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 * @author ZWEI14 GmbH <hallo@ZWEI14.de>
 */
class ElementTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nLoop\Domain\Model\Element
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nLoop\Domain\Model\Element();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNavTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getNavTitle()
        );
    }

    /**
     * @test
     */
    public function setNavTitleForStringSetsNavTitle()
    {
        $this->subject->setNavTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'navTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getNavIconReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getNavIcon()
        );
    }

    /**
     * @test
     */
    public function setNavIconForStringSetsNavIcon()
    {
        $this->subject->setNavIcon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'navIcon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHeaderReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHeader()
        );
    }

    /**
     * @test
     */
    public function setHeaderForStringSetsHeader()
    {
        $this->subject->setHeader('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'header',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubheaderReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubheader()
        );
    }

    /**
     * @test
     */
    public function setSubheaderForStringSetsSubheader()
    {
        $this->subject->setSubheader('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subheader',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIconReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIcon()
        );
    }

    /**
     * @test
     */
    public function setIconForStringSetsIcon()
    {
        $this->subject->setIcon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'icon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLink()
        );
    }

    /**
     * @test
     */
    public function setLinkForStringSetsLink()
    {
        $this->subject->setLink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'link',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getVariantReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getVariant()
        );
    }

    /**
     * @test
     */
    public function setVariantForStringSetsVariant()
    {
        $this->subject->setVariant('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'variant',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDate1ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate1()
        );
    }

    /**
     * @test
     */
    public function setDate1ForDateTimeSetsDate1()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate1($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date1',
            $this->subject
        );
    }
}
