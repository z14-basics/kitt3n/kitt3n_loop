<?php

namespace KITT3N\Kitt3nLoop\Hooks\Previews;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;

class ElementsPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    )
    {
        if (strpos($row['CType'], 'kitt3nloop_elements') !== false) {

            $sRows = '';

            $table = 'tx_kitt3nloop_domain_model_element';
            $joinTable = 'ttcontent_tx_kitt3nloop_domain_model_element_mm';

            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);

            $statement = $queryBuilder
                ->select('*')
                ->from($table)
                ->join(
                    $table,
                    $joinTable,
                    'elementMM',
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq(
                            'elementMM.uid_local', $row['uid']
                        ),
                        $queryBuilder->expr()->eq(
                            'elementMM.uid_foreign',
                            $queryBuilder->quoteIdentifier($table . '.uid')
                        )
                    )
                )
                ->where(
                    $queryBuilder->expr()->eq($table .'.deleted', 0),
                    $queryBuilder->expr()->eq($table .'.hidden', 0)
                )
                ->orderBy('elementMM.sorting', 'ASC');

            $processedData['aElements'] = $statement->execute()->fetchAll();

            if (count($processedData['aElements']) > 0) {
                foreach ($processedData['aElements'] as $aElement) {

                    $sRows .=
                        '<tr>
                        <td>
                            <div>
                                <strong>'. $aElement['uid'] .'</strong>
                            </div>
                        </td>
                        <td>
                            <div>
                                <strong>'. $aElement['pid'] .'</strong>
                            </div>
                        </td>
                        <td>
                            <div>
                                <strong>'. $aElement['nav_title'] .'</span>
                            </div>
                        </td>
                        <td>
                            <div>
                                <strong>'. $aElement['header'] .'</span>
                            </div>
                        </td>
                    </tr>';

                }
            }

            $aTitles = explode("_", $row['CType']);

            $sContentHtml = '            
            <div class=""> 
                <strong>Kitt3n | Loop | ' . (isset($aTitles[2]) ? ucfirst($aTitles[2]) : 'Generic') . '</strong><br>
                <table class="table table-condensed" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th>
                                <strong>UID</strong>                               
                            </th> 
                            <th>
                                <strong>PID</strong>                               
                            </th> 
                            <th>
                                <strong>NAV TITLE</strong>                               
                            </th> 
                            <th>
                                <strong>HEADER</strong>                               
                            </th>                           
                        </tr>
                    </thead>
                    <tbody>            
                        ' . $sRows . '                 
                    </tbody>
                </table>
            </div>            
            ';

            $itemContent .= $sContentHtml;
            $drawItem = false;
        }
    }
}