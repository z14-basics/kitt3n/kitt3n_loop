<?php
namespace KITT3N\Kitt3nLoop\ViewHelpers;

/***
 *
 * This file is part of the "kitt3n_loop" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Return html for column based grid elements
 */
class RenderStaticElementsTabsViewHelper extends AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('aElements', 'array', 'Elements array (Database rows).', true);
        $this->registerArgument('oElements', 'array', 'Elements array (Objects).', true);
        $this->registerArgument('aData', 'array', 'Content element array.', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {

        $aHtml = [];

        $sSectionIdentifier = 'elements' . $arguments['aData']['uid'];

        $aHtml[] = '<section id="' . $sSectionIdentifier . '" class="tx-kitt3n-loop tx-kitt3n-loop--tabs">';

        if (count($arguments['aElements']) > 1) {

            /*
             * nav
             */
            $aHtml[] = '<nav class="row">';

            $i = 0;
            foreach ($arguments['aElements'] as $aElement) {

                $sArticleIdentifier = $sSectionIdentifier . '_element' . $aElement['uid'];

                $aHtml[] = '<div class="col tab">';
                $aHtml[] = '<input type="radio" name="' . $sSectionIdentifier . '-label-radio" class="label-radio" id="nav_' . $sArticleIdentifier . '" ' . ($i === 0 ? "checked" : "") .' />';
                $aHtml[] = '<label class="nav-label' . ($aElement['nav_icon'] == "" ? "" : " icon icon--" . $aElement['nav_icon']) . '" for="nav_' . $sArticleIdentifier . '">';
                $aHtml[] = $aElement['nav_title'];
                $aHtml[] = '</label>';
                $aHtml[] = '</div>';
                $i++;

            }

            $aHtml[] = '</nav>';

        }

        $aHtml[] = '<section>' . $renderChildrenClosure() . '</section>';

        $aHtml[] = '</section>';

        return implode("", $aHtml);
    }

}