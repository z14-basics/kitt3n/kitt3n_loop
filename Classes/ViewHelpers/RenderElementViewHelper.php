<?php
namespace KITT3N\Kitt3nLoop\ViewHelpers;

/***
 *
 * This file is part of the "kitt3n_layouts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use KITT3N\Kitt3nUserfuncs\UserFunc\SettingsUserFunc;

/**
 * Return html for column based grid elements
 */
class RenderElementViewHelper extends AbstractViewHelper {

    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('aElements', 'array', 'Elements array (Database rows).', true);
        $this->registerArgument('oElements', 'array', 'Elements array (Objects).', true);
        $this->registerArgument('aData', 'array', 'Content element array.', true);
        $this->registerArgument('sTemplate', 'string', 'Template for element content.', true);
    }

    /**
     *
     * @return string
     *
     * Use e.g.:
     * <html xmlns:kl="http://typo3.org/ns/KITT3N/Kitt3nLayouts/ViewHelpers">
     *
     * or
     *
     * {namespace kl=KITT3N\Kitt3nLayouts\ViewHelpers}
     *
     * and
     *               
     * <f:variable name="sTemplate" value="{f:render(partial: 'Kitt3nLoopElements/Templates/Tabs/Element')}" />
     * {kitt3nLoop:renderElement(oElements: '{oElements}', aElements: '{aElements}', aData: '{data}', sTemplate: '{sTemplate}') -> f:format.raw()}
     */
    public function render()
    {
        $aHtml = [];

        $sSectionIdentifier = 'tabs' . $this->arguments['aData']['uid'];

        $aHtml[] = '<section id="' . $sSectionIdentifier . '" class="tx-kitt3n-loop tx-kitt3n-loop--tabs">';

        /*
         * nav
         */
        $aHtml[] = '<nav>';
        $aHtml[] = '<ul class="row">';
        foreach ($this->arguments['aElements'] as $i => $aElement) {
            $aHtml[] = '<li class="col">';
            $aHtml[] = '<input type="radio" name="' . $sSectionIdentifier . '-label-radio" class="label-radio" id="' . $sSectionIdentifier . '-label-radio--' . $i . '" ' . ($i === 0 ? "checked" : "") .' />';
            $aHtml[] = '<label class="tab-label" for="' . $sSectionIdentifier . '-label-radio--' . $i . '">';
            $aHtml[] = $aElement['nav_title'];
            $aHtml[] = '</label>';
            $aHtml[] = '</li>';
        }
        $aHtml[] = '</ul>';
        $aHtml[] = '</nav>';

        /*
         * articles
         */
        $aHtml[] = '<section>';
        foreach ($this->arguments['aElements'] as $i => $aElement) {
            $aHtml[] = '<input type="radio" name="' . $sSectionIdentifier . '-article-radio" class="article-radio" id="' . $sSectionIdentifier . '-article-radio--' . $i . '" ' . ($i === 0 ? "checked" : "") .' />';
            $aHtml[] = '<article id="' . $sSectionIdentifier . '-tab--' . $i . '">';

            $sTemplate = $this->arguments['sTemplate'];

            $aFields = [
                'header',
                'subheader',
                'text',
                'link',
                'processedImage',
            ];

            foreach ($aFields as $aField) {

                $bMatch = preg_match_all('|\\[' . $aField . '\\](.+)\\[/' . $aField . '\\]|s', $this->arguments['sTemplate'], $aMatch);
                if ($bMatch) {
                    if ($this->arguments['aElements'][$i][$aField] !== '' && $this->arguments['aElements'][$i][$aField] !== null) {
                        $sTemplate = str_replace('###' . $aField . '###', $this->arguments['aElements'][$i][$aField], $sTemplate);
                        $sTemplate = str_replace(['[' . $aField . ']','[/' . $aField . ']'], ['', ''], $sTemplate);
                    } else {
                        $sTemplate = str_replace($aMatch[0], '', $sTemplate);
                    }
                }

            }

//            $bHeader = preg_match('|\\[header\\](.+)\\[/header\\]|s', $this->arguments['sTemplate'], $aHeader);
//            if ($bHeader) {
//                if ($this->arguments['aElements'][$i]['header'] !== '') {
//                    $sTemplate = str_replace('###header###', $this->arguments['aElements'][$i]['header'], $sTemplate);
//                    $sTemplate = str_replace(['[header]','[/header]'], ['', ''], $sTemplate);
//                } else {
//                    $sTemplate = str_replace($aHeader[0], '', $sTemplate);
//                }
//            }
//
//            $bSubheader = preg_match('|\\[subheader\\](.+)\\[/subheader\\]|s', $this->arguments['sTemplate'], $aSubheader);
//            if ($bSubheader) {
//                if ($this->arguments['aElements'][$i]['subheader'] !== '') {
//                    $sTemplate = str_replace('###subheader###', $this->arguments['aElements'][$i]['subheader'], $sTemplate);
//                    $sTemplate = str_replace(['[subheader]','[/subheader]'], ['', ''], $sTemplate);
//                } else {
//                    $sTemplate = str_replace($aSubheader[0], '', $sTemplate);
//                }
//            }

            $aHtml[] = $sTemplate;
            $aHtml[] = '</article>';
        }
        $aHtml[] = '</section>';

        $aHtml[] = '</section>';

        return implode('', $aHtml);
    }

}