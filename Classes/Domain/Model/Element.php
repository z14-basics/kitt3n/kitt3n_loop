<?php
namespace KITT3N\Kitt3nLoop\Domain\Model;


/***
 *
 * This file is part of the "kitt3n | Loop" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * Element
 */
class Element extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * navTitle
     * 
     * @var string
     * @validate NotEmpty
     */
    protected $navTitle = '';

    /**
     * header
     * 
     * @var string
     */
    protected $header = '';

    /**
     * subheader
     * 
     * @var string
     */
    protected $subheader = '';

    /**
     * text
     * 
     * @var string
     */
    protected $text = '';

    /**
     * image
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * icon
     * 
     * @var string
     */
    protected $icon = '';

    /**
     * link
     * 
     * @var string
     */
    protected $link = '';

    /**
     * variant
     * 
     * @var string
     */
    protected $variant = '';

    /**
     * date
     * 
     * @var \DateTime
     */
    protected $date = null;

    /**
     * date1
     * 
     * @var \DateTime
     */
    protected $date1 = null;

    /**
     * navIcon
     * 
     * @var string
     */
    protected $navIcon = '';

    /**
     * Returns the header
     * 
     * @return string $header
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Sets the header
     * 
     * @param string $header
     * @return void
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * Returns the subheader
     * 
     * @return string $subheader
     */
    public function getSubheader()
    {
        return $this->subheader;
    }

    /**
     * Sets the subheader
     * 
     * @param string $subheader
     * @return void
     */
    public function setSubheader($subheader)
    {
        $this->subheader = $subheader;
    }

    /**
     * Returns the text
     * 
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     * 
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the image
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the icon
     * 
     * @return string $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets the icon
     * 
     * @param string $icon
     * @return void
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * Returns the link
     * 
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Sets the link
     * 
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Returns the variant
     * 
     * @return string $variant
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * Sets the variant
     * 
     * @param string $variant
     * @return void
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    /**
     * Returns the navTitle
     * 
     * @return string $navTitle
     */
    public function getNavTitle()
    {
        return $this->navTitle;
    }

    /**
     * Sets the navTitle
     * 
     * @param string $navTitle
     * @return void
     */
    public function setNavTitle($navTitle)
    {
        $this->navTitle = $navTitle;
    }

    /**
     * Returns the date
     * 
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     * 
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the date1
     * 
     * @return \DateTime $date1
     */
    public function getDate1()
    {
        return $this->date1;
    }

    /**
     * Sets the date1
     * 
     * @param \DateTime $date1
     * @return void
     */
    public function setDate1(\DateTime $date1)
    {
        $this->date1 = $date1;
    }

    /**
     * Returns the navIcon
     * 
     * @return string $navIcon
     */
    public function getNavIcon()
    {
        return $this->navIcon;
    }

    /**
     * Sets the navIcon
     * 
     * @param string $navIcon
     * @return void
     */
    public function setNavIcon($navIcon)
    {
        $this->navIcon = $navIcon;
    }
}
