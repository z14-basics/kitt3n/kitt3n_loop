<?php
namespace KITT3N\Kitt3nLoop\Domain\Repository;

use KITT3N\Kitt3nUserfuncs\Domain\Repository\AbstractRepository;

/***
 *
 * This file is part of the "kitt3n | Loop" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * The repository for Elements
 */
class ElementRepository extends \KITT3N\Kitt3nUserfuncs\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'KITT3N\\Kitt3nLoop\\Domain\\Model\\Element';
        $sUserFuncPlugin = 'tx_kitt3nloop';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
